﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Archivos();
            //Minmax();
            //Rectangulo();
            //Rectanguloa();
            //Rectangulob();
            //Dospuntos();
            //Frase();
            //Factorial();
            //Palindromo();
            //Encript();
            //Punterito();
            //Potencia();




        }
        public static void Archivos()
        {
            FileStream F = new FileStream("Archivo1.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            String cadena = "patata";

            int veces = 0;
            for (int i = 0; i < cadena.Length; i++)
            {
                F.WriteByte((byte)cadena[i]);
            }
            F.Position = 0;

            for (int i = 0; i < cadena.Length; i++)
            {
                if (cadena[i] == 't')
                {
                    veces++;
                }
            }


            Console.WriteLine($"La t aparece {veces} veces");

            F.Close();
        }

        public static void Minmax()
        {
            int[] arraynum = { -120, 7, 32, 143, 1002 };
            int min, max;

            min = max = arraynum[0];

            for ( int i = 0; i<arraynum.Length; i++)
            {
                if (arraynum[i] < min)
                {
                    arraynum[i] = min;
                } else if (arraynum[i] > max)
                {
                    max = arraynum[i];
                }
            }
            Console.WriteLine($"Los numeros máximo y mínimo del array son: \nMin: {min} \nMax:{max}");
                

        }

        public static void Rectangulo()
        {
            int alto;
            Console.WriteLine("Introduzca el alto del rectángulo: ");
            alto = int.Parse(Console.ReadLine());
            int ancho;
            Console.WriteLine("Introduzca el ancho del rectángulo: ");
            ancho = int.Parse(Console.ReadLine());




            string[,] matriz = new string[alto, ancho];

            for (int i = 0; i < matriz.GetLength(0); i++)
            {
                for (int j = 0; j < matriz.GetLength(1); j++)
                {
                    Console.Write(matriz[i, j] = "*");
                }Console.Write(Environment.NewLine + Environment.NewLine);
            }

            Console.WriteLine();

        }

        public static void Rectanguloa()
        {
            int alto;
            Console.WriteLine("Introduzca el alto del rectángulo: ");
            alto = int.Parse(Console.ReadLine());
            int ancho;
            Console.WriteLine("Introduzca el ancho del rectángulo: ");
            ancho = int.Parse(Console.ReadLine());

            string[,] matriz = new string[alto, ancho];

            char caracter;
            Console.WriteLine("Introduzca el caracter con el que quiere hacer el rectángulo: ");
            caracter = char.Parse(Console.ReadLine());

            for (int i = 0; i < matriz.GetLength(0); i++)
            {
                for (int j = 0; j < matriz.GetLength(1); j++)
                {
                    Console.Write(matriz[i, j] = caracter.ToString());
                }
                Console.Write(Environment.NewLine + Environment.NewLine);
            }

            Console.WriteLine();
        }

        public static void Rectangulob()
        {
            int alto;
            Console.WriteLine("Introduzca el alto del rectángulo: ");
            alto = int.Parse(Console.ReadLine());
            int ancho;
            Console.WriteLine("Introduzca el ancho del rectángulo: ");
            ancho = int.Parse(Console.ReadLine());

            string[,] matriz = new string[alto, ancho];

            char parametro;
            Console.WriteLine("Introduzca el parametro con el que quiere hacer el rectángulo: ");
            parametro = char.Parse(Console.ReadLine());

            char color;
            Console.WriteLine("Introduzca el color que quiere que tengan los parametros del rectángulo: ");
            Console.WriteLine("Magenta: m\nAmarillo: a\nVerde: v\nRojo: r");
            color = char.Parse(Console.ReadLine());
            

            Console.BackgroundColor = ConsoleColor.Black;
            switch (color)
            {
                case 'm':
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    break;
                case 'a':
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case 'v':
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case 'r':
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
            }

            for (int i = 0; i < matriz.GetLength(0); i++)
            {
                for (int j = 0; j < matriz.GetLength(1); j++)
                {
                    Console.Write(matriz[i, j] = parametro.ToString());
                }
                Console.Write(Environment.NewLine + Environment.NewLine);
            }

            Console.WriteLine();
            Console.ResetColor();
        }


        public struct Puntos2d
        {
            public byte red;
            public byte blue;
            public byte green;
            public float alpha;
            public short coordx1;
            public short corrdy1;
            public short coordx2;
            public short corrdy2;
        }

        public static void Dospuntos()
        {
            Console.WriteLine("Introduzca los datos relativos a los dos puntos: ");
            Console.WriteLine("Coordenada x del primer punto = ");
            short coordx1 = short.Parse(Console.ReadLine());
            Console.WriteLine("Coordenada Y del primer punto = ");
            short coordy1 = short.Parse(Console.ReadLine());
            Console.WriteLine("Coordenada x del segundo punto = ");
            short coordx2 = short.Parse(Console.ReadLine());
            Console.WriteLine("Coordenada Y del segundo punto = ");
            short coordy2 = short.Parse(Console.ReadLine());

            int [] a = { coordx1, coordy1 };
            int[] b = { coordx2, coordy2 };
            double distancia;

            distancia = Math.Sqrt(Math.Pow((coordx2 - coordx1), 2) + Math.Pow((coordy2 - coordy1), 2));

            Console.WriteLine($"La distancia entre los dos puntos es: {distancia}");



        }

        public static void Frase()
        {
            string frase1;
            Console.WriteLine("Introduzca su frase: ");
            frase1 = Console.ReadLine();

            char opcion;
            Console.WriteLine("Decida que quiere hacer: ");
            Console.WriteLine("Sustituir espacios: s\nVocales en mayúsculas: v\nTransformar a ascii numérico: a");
            opcion = char.Parse(Console.ReadLine());

            switch (opcion)
            {
                case 's':
                    frase1 = frase1.Replace(" ", "_");
                    break;
                case 'v':
                    frase1 = frase1.Replace("a", "A");
                    frase1 = frase1.Replace("e", "E");
                    frase1 = frase1.Replace("i", "I");
                    frase1 = frase1.Replace("o", "O");
                    frase1 = frase1.Replace("u", "U");
                    break;
                case 'a':
                    foreach(char letra in frase1)
                    {
                        Console.WriteLine((int)letra);
                    }
                    break;


            }

            Console.WriteLine($"{frase1}");
        }

        public static void Factorial()
        {
            int fact = 1;
            int resultado = 1;
            Console.WriteLine("Introduzca el numero del que quiere saber el factorial: ");
            fact = int.Parse(Console.ReadLine());
            for (int i = 1; i<=fact; i++)
            {
                resultado = resultado * i;
            }
            Console.WriteLine($"El factorial de {fact} es: {resultado}");


        }

        public static void Palindromo()
        {
            String palin;
            var palin2= new StringBuilder();

            Console.WriteLine("Introduza la palabra que desea saber si es o no palíndromo: ");
            palin = Console.ReadLine();

            int longi = palin.Length;

            for (int i = 0; i < longi; i++)
            {
                palin2.Append(palin[longi - i - 1]);
            }
            Console.WriteLine(palin2);

            if (palin.ToString()==palin2.ToString())
            {
                Console.WriteLine($"La palabra {palin} es un palíndromo");
            }
            else
            {
                Console.WriteLine($"{palin} no es un palíndromo");
            }
        }

        public static void Encript()
        {
            string palabra;
            var palabrasci = new StringBuilder();
            var palabrasci2 = new StringBuilder();
            var palabraptada = new StringBuilder();
            var palabraptada1 = new StringBuilder();
            Console.WriteLine("Introduzca la palabra a codificar: ");
            palabra = Console.ReadLine();
            

            for (int i = 0; i<palabra.Length; i++)
            {
                palabrasci.Append((int)palabra[i]);
                palabrasci2.Append(palabra[i] + 1);
            }
            Console.WriteLine(palabrasci);

            Console.WriteLine(palabrasci2);

            for (int i = 0; i < palabra.Length; i++)
            {
                palabraptada.Append((char)(palabra[i] + 1));
            }
            Console.WriteLine($"La palabra encriptada es: {palabraptada}");

            for (int i = 0; i < palabra.Length; i++)
            {
                palabraptada1.Append((char)(palabraptada[i]-1));
            }
            Console.WriteLine($"La palabra desencripitada es: {palabraptada1}");

        }

        public static void Punterito()
        {
            string frasecilla;
            int y;
            int x;
            Console.WriteLine("Coordenada x: ");
            x = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Coordenada y: ");
            y = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Introduzca su frase: ");
            frasecilla = Console.ReadLine();

            Console.SetCursorPosition(x, y);
            Console.Write(frasecilla);

            
        }

        public static void Potencia()
        {
            int bas;
            int expon;

            Console.WriteLine("Introduzca la base: ");
            bas = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca el exponente: ");
            expon = Int32.Parse(Console.ReadLine());
            int result= 1;

            for (int i = 0; i<expon; i++)
            {
                result *= bas;
            }Console.WriteLine($"El resultado es: {result}");

        }

    }

}
